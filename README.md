# Turbine
## What is it?
Turbine is a tool that helps to automate the transfer of Steam game content across your local network.

It is built using Java.

## Why was it built?
Most modern AAA games are massive, occasionally consuming tens of gigabytes of space each. Downloading these enormous games over an internet connection can be extremely slow and wasteful, especially when multiple devices are attempting to download a game simultaneously.

It is possible however, to download a game once, and then manually copy the files to another device using traditional file transfer standards, such as USB devices or networked file-sharing. This can be very tedious and time-consuming.

Turbine aims to remedy this by allowing a user to see all the games currently installed across the network, and quickly and easily transfer a selected game to their own device over their (often **much** faster) LAN connection, bypassing the need to download the same files again over an internet connection.

As of the time of writing, there exists no tool for automating this transfer of game content across local networks.

## What can it do?
It is currently a WIP, and some features are not implemented yet. In the future, it will be able to:
- ~~List games installed on a device~~ DONE!
- ~~Transfer a list to another device~~ DONE!
- ~~Retrieve a selected game from another device~~ DONE!
- Begin game installation and file discovery using Steam's API