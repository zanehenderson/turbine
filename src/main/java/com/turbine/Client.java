package com.turbine;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.util.List;

public class Client {
    final private String _ipAddressPattern = "^(((2((5[0-5])|[0-4][0-9]))|(1?[0-9]?[0-9]))\\.){3}((2((5[0-5])|[0-4][0-9]))|(1?[0-9]?[0-9]))$";
    final private String _numericPattern = "^[0-9]+$";

    final private int _port;
    final private DataProvider _dataProvider;
    private Socket _server;
    private String _host;

    private boolean _connected;

    private BufferedReader _input;
    private DataInputStream _dataInput;
    private PrintWriter _output;

    public Client(int port, DataProvider dataProvider) {
        System.out.println("Starting client");
        _port = port;
        _dataProvider = dataProvider;

        do {
            _host = getUserInput("Enter server IP address:", _ipAddressPattern);
            if (_host == null)
                continue;
            connect();
        } while (!_connected);
    }

    public void run() {
        // TODO: Allow option to quit gracefully
        while (true) {
            getGameList();
            getGameFiles();
        }
    }

    private String getUserInput(String prompt, String regexPattern) {
        String result;

        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        do {
            System.out.println(prompt);
            try {
                result = input.readLine();
            } catch (Exception e) {
                System.err.println("Failed to get user input");
                return null;
            }
        } while (!result.matches(regexPattern));
        return result;
    }

    private int getUserInputInteger(String prompt) {
        String input = getUserInput(prompt, _numericPattern);
        if (input == null)
            return -1;

        return Integer.parseInt(input);
    }

    private int getUserInputIntegerRange(String prompt, int min, int max) {
        int result;
        do {
            result = getUserInputInteger(String.format("%s (%d to %d)", prompt, min, max));
        } while (result < min || result > max);

        return result;
    }

    private void connect() {
        if (_connected) {
            System.err.println("Server already connected");
            return;
        }

        try {
            _server = new Socket(_host, _port);
            _connected = true;
        } catch (ConnectException e) {
            System.err.printf("Failed to open connection to %s:%d\n", _host, _port);
            return;
        } catch (IOException e) {
            System.err.println("Failed to open server socket");
            return;
        }

        try {
            _input = new BufferedReader(new InputStreamReader(_server.getInputStream()));
            _dataInput = new DataInputStream(_server.getInputStream());
        } catch (IOException e) {
            System.err.println("Failed to get server input stream");
            disconnect();
            return;
        }

        try {
            _output = new PrintWriter(_server.getOutputStream(), true);
        } catch (IOException e) {
            System.err.println("Failed to get server output stream");
            disconnect();
        }
    }

    private void disconnect() {
        if (!_connected) {
            System.err.println("Cannot disconnect: Server isn't connected");
            return;
        }

        try {
            _server.close();
        } catch (IOException e) {
            System.err.println("Couldn't close server socket. Was it already closed?");
        }
        _connected = false;
    }

    private boolean sendMessage(String message) {
        if (!_connected) {
            System.err.println("Cannot send message: Server isn't connected");
            return false;
        }

        _output.println(message);
        return true;
    }

    private String receiveMessage() {
        if (!_connected) {
            System.err.println("Cannot receive message: Server isn't connected");
            return null;
        }

        try {
            return _input.readLine();
        } catch (IOException e) {
            System.err.println("Failed to receive message from server");
            return null;
        }
    }

    private boolean receiveAndCompareMessage(String expectedMessage) {
        String message = receiveMessage();

        if (message == null) {
            return false;
        } else if (!message.equals(expectedMessage)) {
            System.err.printf("Unexpected response from server: %s\nExpected: %s", message, expectedMessage);
            return false;
        }

        return true;
    }

    private String humanReadableBytes(long bytesValue) {
        if (bytesValue < 1024)
            return String.format("%d B", bytesValue);

        String[] postfixes = {"K", "M", "G", "T", "P", "E"};
        double decimal = bytesValue;
        for (String postfix : postfixes) {
            decimal /= 1024.0;
            if (decimal < 1024)
                return String.format("%.1f %siB", decimal, postfix);
        }

        return ">1 ZiB";
    }

    private void getGameList() {
        connect();
        if (!_connected)
            return;

        if (!receiveAndCompareMessage("LISTENING")) {
            disconnect();
            return;
        }

        if (!sendMessage("LIST"))
            return;

        if (!receiveAndCompareMessage("STARTLIST")) {
            disconnect();
            return;
        }

        System.out.printf("%-7s | %-64s | %-9s\n", "ID", "Name", "Size");
        while (true) {
            String message = receiveMessage();
            if (message == null)
                continue;
            if (!message.equals("GAME"))
                break;

            String gameID, gameName, gameSize;
            gameID = receiveMessage();
            gameName = receiveMessage();
            gameSize = receiveMessage();

            if (gameID == null || gameName == null || gameSize == null)
                break;

            System.out.printf("%7s | %-64s | %9s\n",
                    gameID, gameName, humanReadableBytes(Long.parseLong(gameSize)));
        }
        System.out.println("-----");
        disconnect();
    }

    private void getGameFiles() {
        String chosenGame = getUserInput("Which game would you like to get?", _numericPattern);

        System.out.printf("%-10s | %-96s\n", "ID", "Path");
        List<String> libraries = _dataProvider.getLibraries();
        int libraryID = 0;
        for (String library : libraries) {
            System.out.printf("%-10s | %-96s\n", libraryID, library);
            libraryID += 1;
        }
        System.out.println("-----");

        int chosenLibrary = getUserInputIntegerRange(
                "Which library would you like to install to?",
                0, libraries.size());

        String libraryDir = libraries.get(chosenLibrary) + "\\common";

        connect();
        if (!_connected)
            return;

        if (!receiveAndCompareMessage("LISTENING")) {
            disconnect();
            return;
        }

        if (!sendMessage("GET"))
            return;
        if (!sendMessage(chosenGame))
            return;

        if (!receiveAndCompareMessage("STARTGET")) {
            disconnect();
            return;
        }

        String installFolder = receiveMessage();
        if (installFolder == null) {
            disconnect();
            return;
        }

        File dir = new File(libraryDir);

        boolean succeeded = true;
        String filePath, fileSizeString;
        while (true) {
            String message = receiveMessage();
            if (message == null) {
                succeeded = false;
                break;
            }
            if (message.equals("ENDGET"))
                break;

            filePath = receiveMessage();
            fileSizeString = receiveMessage();

            if (filePath == null || fileSizeString == null) {
                System.err.println("Failed to get file details");
                succeeded = false;
                break;
            }

            long fileSize = Long.parseLong(fileSizeString);


            System.out.printf("%s (%s)\n", filePath, humanReadableBytes(fileSize));

            File file = new File(dir, filePath);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }

            try {
                if (!file.createNewFile()) {
                    System.err.println("File already exists");
                    if (!sendMessage("SKIP"))
                        return;
                    continue;
                }
            } catch (IOException e) {
                System.err.printf("Failed to create file: %s\n", filePath);
                succeeded = false;
                break;
            }

            sendMessage("READY");

            try {
                FileOutputStream fileOutput = new FileOutputStream(file);
                byte[] buffer = new byte[1024*1024];

                int read;
                long remaining = fileSize;
                while((read = _dataInput.read(buffer, 0,
                        remaining < buffer.length ? (int)remaining : buffer.length)) > 0) {
                    remaining -= read;
                    fileOutput.write(buffer, 0, read);
                }

                fileOutput.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (succeeded) {
            System.out.println("\nDone!");
            System.out.println("You can now begin installation via Steam\n");
        } else {
            System.out.println("An error occurred. Game transfer may not be complete.\n");
        }

        disconnect();
    }
}