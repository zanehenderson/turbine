package com.turbine;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonObject.Member;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.nio.file.Paths;
import java.util.*;

public class DataProvider {
    private final List<String> _libraries;
    private final List<GameDetail> _gameDetails;

    //TODO: Allow for dynamic Steam path
    private final String _steamPath = "C:\\\\Program Files (x86)\\\\Steam";
    private final String _steamappsPath = _steamPath + "\\\\steamapps";
    private final String _vdfPath = _steamappsPath + "\\\\libraryfolders.vdf";

    public DataProvider() {
        _libraries = new ArrayList<>();
        _gameDetails = new ArrayList<>();

        populateLibraries();
        populateGames();
    }

    public List<String> getLibraries() { return _libraries; }
    public List<GameDetail> getGames() {
        return _gameDetails;
    }

    public GameDetail getGame(int gameID) {
        for (GameDetail game : _gameDetails) {
            if (game.getSteamID() == gameID) {
                return game;
            }
        }
        return null;
    }

    public Collection<File> getGameFiles(int gameID) {
        GameDetail game = getGame(gameID);

        if (game == null)
            return null;

        File rootDir = new File(Paths.get(game.getLibraryPath() + "/common/" + game.getInstallFolder()).toString());

        return FileUtils.listFiles(rootDir, null, true);
    }

    private void populateLibraries() {
        // Steam may be using multiple drives for storage of games, so we need to identify where these paths are.
        // These paths are kept in a file called 'libraryfolders.vdf'.

        JsonObject libsJSON = Json.parse(readVDFtoJSON(new File(_vdfPath))).asObject()
                .get("LibraryFolders").asObject();

        // Add the default Steam library
        _libraries.add(_steamappsPath);

        // Additional libraries are listed as ("1", "2", ... "n")
        // We need to identify and store the values of any key which is numeric.
        for (Member member : libsJSON) {
            String name = member.getName();
            if (StringUtils.isNumeric(name)) {
                String path = member.getValue().toString().replace("\"", "") + "\\\\steamapps";
                _libraries.add(path);
            }
        }
    }

    private void populateGames() {
        // There can be multiple Steam libraries
        for (String library: _libraries) {
            // We need to know what games are installed
            // We can do this by reading any file named "appmanifest_<id>.acf"
            File[] files = new File(library).listFiles((file, s) -> s.matches("appmanifest_[0-9]+.acf"));
            if (files == null)
                break;

            // Each ACF file contains some metadata that we need to keep track of
            // The files are in the VDF format
            for (File acfFile : files) {
                try {
                    JsonObject acfJSON = Json.parse(readVDFtoJSON(acfFile)).asObject()
                            .get("AppState").asObject();

                    GameDetail game = new GameDetail(
                            library,      // Library ID
                            Integer.parseInt(acfJSON.getString("appid", "").replace("\"", "")),     // ID
                            acfJSON.getString("name", ""),                                          // Name
                            acfJSON.getString("installdir", ""),                                    // Install folder
                            Long.parseLong(acfJSON.getString("SizeOnDisk", "").replace("\"", ""))   // Installed size
                    );

                    _gameDetails.add(game);
                } catch (Exception e) {
                    if (acfFile.length() == 0)
                        System.err.printf("%s: File is empty!\n", acfFile.toString());
                    else
                        System.err.printf("%s: Bad encoding!\n", acfFile.toString());
                }
            }
        }
        _gameDetails.sort(Comparator.comparingInt(GameDetail::getSteamID));
    }

    private String readVDFtoJSON(File vdfFile) {
        // Steam uses a proprietary data format similar to JSON.
        // We can coerce the VDF data into JSON using basic string manipulation

        StringBuilder sb = new StringBuilder();
        sb.append("{\n");
        try (BufferedReader br = new BufferedReader(new FileReader(vdfFile))) {
            // Read the file line by line
            String line = br.readLine();
            while (line != null) {
                String nextLine = br.readLine();

                int count = StringUtils.countMatches(line, "\"");
                if (count == 2) {
                    sb.append(line)
                            .append(':');
                } else if (count == 4) {
                    int splitIndex = StringUtils.ordinalIndexOf(line, "\"",2) + 1;
                    sb.append(line, 0, splitIndex)
                            .append(':')
                            .append(line.substring(splitIndex));
                    if (!nextLine.trim().equals("}")) {
                        sb.append(',');
                    }
                } else if (line.trim().equals("{")) {
                    sb.append(line);
                } else if (line.trim().equals("}")) {
                    sb.append(line);
                    if (nextLine != null) {
                        if (!nextLine.trim().equals("}")) {
                            sb.append(',');
                        }
                    }
                }
                sb.append('\n');
                line = nextLine;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        sb.append("}\n");
        return sb.toString();
    }
}
