package com.turbine;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server implements Runnable {
    // TODO: Cleanup exceptions

    final private int _port;
    private boolean _isRunning = true;
    private ServerSocket _socket;
    final private ExecutorService _workers;
    final private DataProvider _dataProvider;

    public Server(int port, int workerCount, DataProvider dataProvider) {
        _port = port;
        _dataProvider = dataProvider;
        _workers = Executors.newFixedThreadPool(workerCount);
    }

    public void run() {
        System.out.println("Starting server...");
        try {
            _socket = new ServerSocket(_port);
        } catch (IOException e) {
            System.err.printf("Could not open port %d\n", _port);
            _workers.shutdown();
            return;
        }

        System.out.println("Server running");

        while (_isRunning) {
            Socket client;
            try {
                client = _socket.accept();
            } catch (IOException e) {
                System.err.println("Could not open client socket");
                continue;
            }

            try {
                _workers.execute(new Worker(client, _dataProvider));
            } catch (SocketException e) {
                System.err.println("Failed to create new worker instance");
            }
        }
        _workers.shutdown();
        System.out.println("Server Stopped") ;
    }

    public synchronized void stop(){
        _isRunning = false;
        try {
            _socket.close();
        } catch (IOException e) {
            System.err.println("Failed to close server socket");
        }
    }
}
