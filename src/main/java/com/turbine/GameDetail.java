package com.turbine;

public class GameDetail {

    private String _libraryPath;
    private int _steamID;
    private String _name;
    private String _installFolder;
    private long _sizeOnDisk;


    public GameDetail( String libraryPath, int steamID, String name, String installFolder, long sizeOnDisk) {
        _libraryPath = libraryPath;
        _steamID = steamID;
        _name = name;
        _installFolder = installFolder;
        _sizeOnDisk = sizeOnDisk;
    }

    public int getSteamID() {
        return _steamID;
    }

    public String getName() {
        return _name;
    }

    public String getLibraryPath() { return _libraryPath; }

    public String getInstallFolder() {
        return _installFolder;
    }

    public long getSizeOnDisk() {
        return _sizeOnDisk;
    }

    public String toString() {
        return String.format("{\"game_id\": \"%d\"," +
                        "\"name\": \"%s\"," +
                        "\"install_dir\": \"%s\"," +
                        "\"size\": \"%d\"}",
        _steamID, _name, _installFolder, _sizeOnDisk);
    }
}
