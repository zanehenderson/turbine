package com.turbine;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.util.List;


public class Worker implements Runnable{
    // TODO: Cleanup exceptions
    final private Socket _client;
    final private DataProvider _dataProvider;

    final private BufferedReader _input;
    final private PrintWriter _output;
    final private DataOutputStream _dataOutput;

    public Worker(Socket client, DataProvider dataProvider) throws SocketException {
        _client = client;
        _dataProvider = dataProvider;

        try {
            _output = new PrintWriter(_client.getOutputStream(), true);
            _input = new BufferedReader(new InputStreamReader(_client.getInputStream()));
            _dataOutput = new DataOutputStream(_client.getOutputStream());
        } catch (IOException e) {
            throw new SocketException("Failed to get socket IO streams");
        }
    }

    private void sendMessage(String message) {
        _output.println(message);
    }

    private String receiveMessage() {
        try {
            return _input.readLine();
        } catch (IOException e) {
            System.err.println("Failed to receive message from client");
            return null;
        }
    }

    public void run() {
        sendMessage("LISTENING");

        String clientAction = receiveMessage();
        switch (clientAction == null ? "FAIL" : clientAction) {
            case "LIST": {
                // Client wants a list of available games
                List<GameDetail> games = _dataProvider.getGames();
                sendMessage("STARTLIST");

                for (GameDetail game : games) {
                    sendMessage("GAME");
                    sendMessage(String.valueOf(game.getSteamID()));
                    sendMessage(game.getName());
                    sendMessage(String.valueOf(game.getSizeOnDisk()));
                }

                sendMessage("ENDLIST");
                break;
            }
            case "GET": {
                // Client wants a game
                int gameID;
                try {
                    String clientSelection = receiveMessage();
                    gameID = Integer.parseInt(clientSelection == null? "ERROR" : clientSelection);
                } catch (NumberFormatException e) {
                    sendMessage("ERROR - Bad game ID");
                    break;
                }

                GameDetail game = _dataProvider.getGame(gameID);
                if (game == null) {
                    sendMessage("ERROR - Game doesn't exist");
                    break;
                }

                sendMessage("STARTGET");
                sendMessage(game.getInstallFolder());

                for (File file : _dataProvider.getGameFiles(gameID)) {
                    sendMessage("NEWFILE");
                    String absolutePath = file.getPath();
                    String relativePath = absolutePath.substring(absolutePath.indexOf(game.getInstallFolder()));

                    sendMessage(relativePath);
                    sendMessage(String.valueOf(file.length()));

                    String message = receiveMessage();
                    if (message == null)
                        break;
                    if (message.equals("SKIP"))
                        continue;
                    if (!message.equals("READY"))
                        break;

                    FileInputStream fileInput;
                    try {
                        fileInput = new FileInputStream(file);
                    } catch (FileNotFoundException e) {
                        // TODO: If server fails to open file, need to inform client
                        System.err.printf("Failed to open file %s\n", absolutePath);
                        break;
                    }

                    byte[] buffer = new byte[8192];
                    try {
                        int read;
                        while ((read = fileInput.read(buffer)) > 0) {
                            _dataOutput.write(buffer, 0, read);
                        }

                        fileInput.close();
                    } catch (IOException e) {
                        // TODO: If server fails to send data, need to inform client
                        System.err.println("Failed to send file data to client");
                        break;
                    }
                }
                sendMessage("ENDGET");
                break;
            }
            case "FAIL":
            default:
                // Do nothing
                break;
        }

        try {
            _output.close();
            _dataOutput.close();
            _input.close();
            _client.close();
        } catch (IOException e) {
            System.err.println("Failed to close IO socket streams. Were they already closed?");
        }
    }
}