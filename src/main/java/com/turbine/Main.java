package com.turbine;

public class Main {

    public static void main(String[] args) {
        final int port = 9999;
        final int workerCount = 5;
        final DataProvider dataProvider = new DataProvider();

        Server server = new Server(port, workerCount, dataProvider);
        Thread serverThread = new Thread(server);
        serverThread.start();

        Client c = new Client(port, dataProvider);
        c.run();
        server.stop();
    }
}
